import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { animate, state, style, transition, trigger } from '@angular/animations';



@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  constructor(private http:HttpClient) {
   }

departments:any=[];

modalTitle="";
DepartmentId =0;
DepartmentName="";

display: boolean = false;


  ngOnInit(): void {

this.refreshList();
  }

  refreshList(){
    this.http.get<any>(environment.API_URL+'department').subscribe(
      data=>{
        console.log("imprimiendo la data",data);
        
        this.departments=data;
      }
    );
  }

  addClick(){
    this.display=true;
    this.modalTitle="Add Department";
    this.DepartmentId=0;
    this.DepartmentName="";
  }

  createClick(){
var val={
  DepartmentName:this.DepartmentName
};
  this.http.post(environment.API_URL+'department',val).subscribe(
    res=>{
      alert(res.toString());
      this.refreshList();
    }
  );


  }
  updateClick(){
    var val={
      DepartmentId:this.DepartmentId,
      DepartmentName:this.DepartmentName
    };
      this.http.put(environment.API_URL+'department',val).subscribe(
        res=>{
          alert(res.toString());
          this.refreshList();
        }
      );
    
  }

}
