import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'anglpythdijngo';

   items: MenuItem[]=[];

  ngOnInit() {
      this.items = [
          {label: 'Home', icon: 'pi pi-fw pi-home' ,routerLink:['/home']},
          {label: 'Department', icon: 'pi pi-fw pi-calendar',routerLink:['/department']},
          {label: 'Employee', icon: 'pi pi-fw pi-pencil',routerLink:['/employee']},
          {label: 'Documentation', icon: 'pi pi-fw pi-file'},
          {label: 'Settings', icon: 'pi pi-fw pi-cog'}
      ];
  }
}


